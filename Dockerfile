FROM centos:7
RUN yum update -y && \
    yum install -y python3 python3-pip curl && \
    pip3 install flask flask-jsonpify flask-restful

COPY python_api /python_api
ENTRYPOINT ["python3", "python_api/python-api.py"] 
